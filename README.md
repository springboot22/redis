# Springboot + Redis简单使用案例

### 1、Jedis和RedisTemplate有何区别？

Jedis是Redis官方推荐的面向Java的操作Redis的客户端，而RedisTemplate是SpringDataRedis中对JedisApi的高度封装。
SpringDataRedis相对于Jedis来说可以方便地更换Redis的Java客户端，比Jedis多了自动管理连接池的特性，方便与其他Spring框架进行搭配使用。

- 性能对比
  jedis – 极强
  RedisTemplate – 逊色
- 拓展性对比
  jedis – redis提供官方API 使用时自构建 jedisPool
  RedisTemplate – 基于Spring ，拓展性 极强
- 使用上
  jedis – 十分不优美，看着low ，但十分简单
  RedisTemplate – 比较优美 ， 使用较为复杂

### 2、Jedis和RedisTemplate操作比较

Copy自https://www.cnblogs.com/z-sir/p/13664221.html

|     操作      |                  Jedis                   |                     StringRedisTemplate                      |
| :-----------: | :--------------------------------------: | :----------------------------------------------------------: |
|  **String**   |                                          |                                                              |
|     设置      |               set("k","v")               |             template.opsForValue().set("k","v")              |
|     获取      |                 get("k")                 |               template.opsForValue().get("k")                |
|      增1      |                incr("k")                 |           template.boundValueOps("k").increment(1)           |
|      减1      |                decr("k")                 |          template.boundValueOps("k").increment(-1)           |
|   设置时间    |          setex("k",seconds,"v")          |   template.opsForValue().set("k","v",20, TimeUnit.SECONDS)   |
| 不存在 就设置 |              setnx("k","v")              |         template.opsForValue().setIfAbsent("k", "v")         |
| 获取过期时间  |                 ttl("k")                 |                   template.getExpire("k")                    |
|     删除      |                 del("k")                 |                     template.delete("k")                     |
|   **Hash**    |                                          |                                                              |
|     设置      |     jedis.hset("pig","name","peiqi";     |       template.opsForHash().put("pig","name","peiqi")        |
|     获取      |         jedis.hget("pig","name")         | template.opsForHash().get("pig", "name") 获取所有template.opsForHash().values("pig") |
|     删除      |         jedis.hdel("pig","name")         |          template.opsForHash().delete("pig","name")          |
| 判断是否存在  |       jedis.hexists("pig","name")        |          template.opsForHash().hasKey("pig","name")          |
|   **List**    |              左/右不做区分               |                                                              |
|     添加      |              rpush("k","v")              |           template.opsForList().rightPush("k","v")           |
|     移出      |               rpop("list")               |             template.opsForList().rightPop("k")              |
|     长度      |                llen("k")                 |               template.opsForList().size("k")                |
|     获取      |       lrange("list",0,-1) //-1全部       |          template.opsForList().range("list", 0, -1)          |
|    **Set**    |                                          |                                                              |
|     添加      |              sadd("k","v")               |              template.opsForSet().add("k","v")               |
|    值移除     |              srem("k","v")               |             template.opsForSet().remove("k","v")             |
|    直接移     |                spop("k")                 |                template.opsForSet().pop("k")                 |
|     长度      |                scard("k")                |                template.opsForSet().size("k")                |
|     交集      |            sinter("k1","k2" )            |          template.opsForSet().intersect("k", "k2")           |
|     并集      |            sunion("k1","k2" )            |            template.opsForSet().union("k", "k2")             |
|     差集      |            sdiff("k1","k2" )             |          template.opsForSet().difference("k", "k2")          |
|   **Zset**    |                                          |                                                              |
|     增加      |             zadd("k",1,"a")              |            template.opsForZSet().add("k","aa",12)            |
|   排名结果    |          zrevrange("k", 0, -1)           |        template.opsForZSet().reverseRange("k", 0, -1)        |
|   排名分数    | zrevrangeByScoreWithScores("k", 12, 10); |    template.opsForZSet().reverseRangeByScore("k", 1, 100)    |
|   修改分数    |           zincrby("k",20,"a")            |      template.opsForZSet().incrementScore("k","aa",19)       |
|     数量      |                zcard("k")                |               template.opsForZSet().zCard("k")               |
|   获取排名    |              zrank("k","a")              |             template.opsForZSet().rank("k","aa")             |
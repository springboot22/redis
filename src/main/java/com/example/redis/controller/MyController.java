package com.example.redis.controller;

import com.example.redis.entity.User;
import com.example.redis.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author will.tuo
 * @date 2021/8/9 10:17
 */
@RestController
public class MyController {
    @Autowired
    private RedisUtil redisUtil;
    @RequestMapping("/get")
    public String get(){
        redisUtil.hset("user","name",1);
        System.out.println(">>>>>>>>>>hn"+ redisUtil.hget("user","name"));
        return ">>>>>>>>>>hn"+ redisUtil.hget("user","name");
    }
    @RequestMapping("/")
    public String getIndex(){
        return "hello";
    }
    @RequestMapping("/getList1")
    public List<Object> getList(){
        List<User> stus = new ArrayList<>();
        stus.add(new User("张三",10));
        stus.add(new User("里斯",10));
        stus.add(new User("张啊大哥",10));
        redisUtil.lSet("学生列表",stus);
        return redisUtil.lGet("学生列表",0,-1);
    }
    @RequestMapping("/getList2")
    public List<Object> getList2(){
        List<User> teachers = new ArrayList<>();
        teachers.add(new User("张三",50));
        teachers.add(new User("里斯",60));
        teachers.add(new User("张啊大哥",70));
        redisUtil.lSet("tt",teachers);
        return redisUtil.lGet("tt",0,-1);
    }
}

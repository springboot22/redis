package com.example.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Administrator
 */
@SpringBootApplication
public class Redis01Application {
    public static void main(String[] args) {
        SpringApplication.run(Redis01Application.class, args);
    }

}

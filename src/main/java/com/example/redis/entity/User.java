package com.example.redis.entity;

import lombok.*;

/**
 * @author will.tuo
 * @date 2021/8/9 10:25
 */
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {
    public String name;
    public int age;
}
